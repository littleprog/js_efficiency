在我的电脑上的运行对比(环境 Node v20.3.1, windows 11 Ryzen 7 6800H/32G)

```
Array add 10M elements 3 times (by push) 
time diff:  284
time diff:  363
time diff:  226
Array add 0.1M elements 3 times (by unshift. unshift is really slow, so I use 1% elements of 10M)
time diff:  737
time diff:  703
time diff:  736
Object add 10M elements
time diff:  244
time diff:  294
time diff:  249
 Array delete 10000 elements(by splice from beginning)
time diff:  636
 Object delete 10000 elements
time diff:  1
 Array modify 10M elements 3 times
time diff:  8
time diff:  7
time diff:  8
 Objec modify 10 elements 3 times
time diff:  8
time diff:  8
time diff:  7
```