let ga = [], go = {}
function tr(f) { d1 = new Date(); f(); d2 = new Date(); console.log("time diff: ", d2 - d1); }
function fa() { for (let i = 0; i < 10000000; i++) { ga.push(i) } }
function faUnshift(){ for (let i = 0; i < 100000; i++) { ga.unshift(i) } }
function fo() { for (let i = 0; i < 10000000; i++) { go[i] = i } }
function fad(){for(let i = 0; i < 10000; i++){ga.splice(1,1)}}
function fod(){for(let i = 0; i < 10000; i++){delete go[i]}}
function fam(){for (let i = 0; i < 10000000; i++) {ga[i] = i+1}}
function fom(){for (let i = 0; i < 10000000; i++) {go[i] = i+1}}
console.log('Array add 10M elements 3 times (by push) ')
ga = []
tr(fa) 
ga = []
tr(fa) 
ga = []
tr(fa) 
console.log('Array add 0.1M elements 3 times (by unshift. unshift is really slow, so I use 1% elements of 10M)')
ga = []
tr(faUnshift)
ga = []
tr(faUnshift)
ga = []
tr(faUnshift)
console.log('Object add 10M elements')
go = {}
tr(fo) 
go = {}
tr(fo) 
go = {}
tr(fo) 
console.log(' Array delete 10000 elements(by splice from beginning)')
tr(fad)
console.log(' Object delete 10000 elements')
tr(fod)
console.log(' Array modify 10M elements 3 times')
ga = []
fa()
tr(fam)
tr(fam)
tr(fam)
console.log(' Objec modify 10 elements 3 times')
go = {}
fo()
tr(fom)
tr(fom)
tr(fom)